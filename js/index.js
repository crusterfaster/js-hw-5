class CreateNewUser
{
    constructor() {
        this.firstName = prompt("Type your name");
        this.lastName = prompt("Type your last name");
        this.birthdayDate = prompt("Type your birthday Date","date dd.mm.yyyy");
    }

    getLogin() {
        return `${this.firstName [0].toLowerCase ()}.${this.lastName.toLowerCase ()}`;
    }
    getAge(){
        let today = new Date();
        let userBirthday = Date.parse(`${this.birthdayDate.slice(6)}.${this.birthdayDate.slice(3,5)}.${this.birthdayDate.slice(0, 2)}`);
        let userAge = (today - userBirthday)/ (1000*60*60*24*30*12).toFixed(0);
        if (userAge < today){
            return`You are ${userAge} years old`;
        }
    };
    getPassword(){
        return`${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthdayDate.slice(-4)}`
    };
}

const newUser = new CreateNewUser();
console.log(newUser.getAge());
console.log(newUser.getPassword());




